// Query Operators
// allows us to expand our queries and define conditions instead of looking for specific values

db.products.insertMany([

    {
        name: "Iphone X",
        price: 30000,
        isActive: true
    },
    {
        name: "Samsung Galaxy S21",
        price: 51000,
        isActive: true
    },
    {
        name: "Razer Blackshark V2X",
        price: 2800,
        isActive: false
    },
    {
        name: "RAKK Gaming Mouse",
        price: 1800,
        isActive: true
    },
    {
        name: "Razer Mechanical Keyboard",
        price: 4000,
        isActive: true

    }
])

// $gt - greater than
db.products.find(
    {
        price: {$gt: 3000}
    }
)

// $lt - less than
db.products.find(
    {
        price: {$lt: 3000}
    }
)

// $gte - greater than or equal to
db.products.find(
    {
        price: {$gte: 30000}
    }
)

// $lte - less than or equal to
db.products.find(
    {
        price: {$lte: 2800}
    }
)

// $regex - query operator which allows us to find documents which will match characters we are looking for

// $regex to look for documents with a partial match and by default is case-sensitive:
db.users.find(
    { 
        firstName: {$regex: 'O'}
    }
)

// $options: $i - will make our regex case insensitive
db.users.find(
    {
        firstName:{$regex: 'e', $options: '$i'}
    }
)

// We can also use $regex for partial matches
db.products.find(
    {
        name: {$regex: 'phone', $options:'$i'}
    }
)

db.users.find(
    {
        email: {$regex: 'web', $options:'$i'}
    }
)

// $or $and - logical operators and they work almost the same as in JS

// $or - allows us to set conditions to find documents which can satisfy at least 1 of the given conditions
db.products.find(
    { 
        $or: [
            {
                name:{$regex:'x', $options: '$i'}
            }, 
            {
                price:{$lte: 10000}
            }
        ]
    }
)

db.products.find({ 
    
        $or: [
    
            {name:{$regex:'x', $options: '$i'}}, 
            {price:{$gte: 30000}}
        ]
})


// $and - allows us to set conditions to find documents which can satisfy all of the given conditions
db.products.find(
    { 
        $and: [
            {
                name:{$regex:'razer', $options: '$i'}
            }, 
            {
                price:{$gte: 3000}
            }
        ]
    }
)

// Field projection - allows us to hide/show certain fields and properties of documents that were retrieved.
// field projection = 0 means hide, while 1 means show
// By defulat, we have to explicitly hide the id field

db.users.find({},
    {
        _id:0,
        password:0
    }
)

db.users.find(
    
    {isAdmin:true},
    {_id:0, email:0}
)

db.users.find({isAdmin:false},{_id:0,firstName:1, lastName:1})